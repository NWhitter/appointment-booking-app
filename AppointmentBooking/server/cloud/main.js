
// Use Parse.Cloud.define to define as many cloud functions as you want.
// For example:
Parse.Cloud.define("hello", function(request, response)
{
    response.success("Hello world!");
});

Parse.Cloud.define("getAvailableSlots", function(request, response)
{
	Parse.Cloud.useMasterKey();

    var Doctor = Parse.Object.extend("Doctor");
    var doctor = new Doctor();
    doctor.id = request.params.doctor;

    var appointmentObject = Parse.Object.extend("Appointment");
    var query = new Parse.Query(appointmentObject);
    var appointmentDate = request.params.appointmentDate;
    appointmentDate.setHours(0);
    appointmentDate.setMinutes(0);
    appointmentDate.setSeconds(0);
    appointmentDate.setMilliseconds(0);
	console.log(appointmentDate);
	console.log(doctor);
    query.equalTo("appointmentDate", request.params.appointmentDate);
    query.equalTo("doctor", doctor);
    query.include("slot");
    query.include("doctor");
    query.find().then(function(results)
    {
        console.log("All appointment results : " + results);

        var start = new Array();
        var finish = new Array();


        for (var i = 0; i < results.length; i++)
		{
            var slot = results[i].get("slot");
            start.push(slot.get("startTime"));
            finish.push(slot.get("finishTime"));
            console.log(i + " Start time : " + slot.get("startTime"));
        }

        console.log(start);
        console.log(finish);

        var slotObject = Parse.Object.extend("Slot");
        var innerQuery = new Parse.Query(slotObject);
        innerQuery.notContainedIn("startTime", start);
        innerQuery.notContainedIn("finishTime", finish);
        innerQuery.ascending("startTime");

        return innerQuery.find();
    }).then(function(results)
        {
            response.success(results);
        },
        function (error)
        {
            response.error("no appointments available");
        });
});

Parse.Cloud.beforeSave("Appointment", function(request, response)
{
	if (!request.object.get("patient") || request.object.get("patient") <= 0)
    {
        response.error("patient is required");
    }
	else if (!request.object.get("doctor") || request.object.get("doctor") <= 0)
    {
        response.error("doctor is required");
    }
	else if (!request.object.get("slot") || request.object.get("slot") <= 0)
    {
        response.error("slot is required");
    }
	else if (!request.object.get("appointmentDate") || request.object.get("appointmentDate") <= 0)
    {
        response.error("appointment Date is required");
    }
	
	var query = new Parse.Query("Appointment");

	query.equalTo("doctor", request.object.get("doctor"));
	query.equalTo("slot", request.object.get("slot"));
	var appointmentDate = request.object.get("appointmentDate");
    appointmentDate.setHours(0);
    appointmentDate.setMinutes(0);
    appointmentDate.setSeconds(0);
    appointmentDate.setMilliseconds(0);
	
	query.equalTo("appointmentDate", appointmentDate);
	
	query.first().then(function (object)
	{
		if (object)
		{
			response.error("Appointment is already booked");
		}
		else
		{
			request.object.set("status", "booked");
			response.success();
		}
	},
	function (error)
	{
		response.error("Error saving appointment");
	});
});

Parse.Cloud.beforeSave(Parse.User, function(request, response)
{
    if (!request.object.get("firstName") || request.object.get("firstName") <= 0)
    {
        response.error("name is required");
    }
    else if (!request.object.get("surname") || request.object.get("surname") <= 0)
    {
        response.error("surname is required");
    }
    else if (!request.object.get("dateOfBirth"))
    {
        response.error("date of birth is required");
    }
    else if (!request.object.get("practice") || request.object.get("practice") <= 0)
    {
        response.error("GP practice is required");
    }
    else
    {
        response.success();
    }
});