package com.finalyear.appointmentbooking.app.test.unit.ui;

import android.content.Intent;
import android.test.ActivityUnitTestCase;
import android.test.suitebuilder.annotation.MediumTest;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.finalyear.appointmentbooking.app.R;
import com.finalyear.appointmentbooking.app.data.User;
import com.finalyear.appointmentbooking.app.ui.LoginActivity;

/**
 * @author Natasha Whitter <Natasha Whitter>
 * @version 1.0
 * @since 18/04/2014
 */
public class LoginActivityTest extends ActivityUnitTestCase<LoginActivity>
{
    private Intent intent;

    public LoginActivityTest()
    {
        super(LoginActivity.class);
    }

    @Override
    protected void setUp() throws Exception
    {
        super.setUp();
        intent = new Intent(getInstrumentation().getTargetContext(), LoginActivity.class);
    }

    /**
     * Test views in the activity aren't null
     */
    @MediumTest
    public void testPreconditions()
    {
        startActivity(intent, null, null);
        final Button btnLogin = (Button) getActivity().findViewById(R.id.account_login);
        final TextView tvCreateAccount = (TextView) getActivity().findViewById(R.id.account_registration);
        final TextView tvPasswordReset = (TextView) getActivity().findViewById(R.id.account_password_recovery);
        final EditText etEmail = (EditText) getActivity().findViewById(R.id.account_email);
        final EditText etPassword = (EditText) getActivity().findViewById(R.id.account_password);

        assertNotNull("Activity is null" + getActivity());
        assertNotNull("Login button is null" + btnLogin);
        assertNotNull("Registration textview is null" + tvCreateAccount);
        assertNotNull("Password textview is null" + tvPasswordReset);
        assertNotNull("Email edittext is null" + etEmail);
        assertNotNull("Password edittext is null" + etPassword);
    }

    /**
     * Tests the registration activity has lunched successfully
     */
    @MediumTest
    public void testRegistrationActivityLaunchedWithIntent()
    {
        startActivity(intent, null, null);
        final TextView tvCreateAccount = (TextView) getActivity().findViewById(R.id.account_registration);
        tvCreateAccount.performClick();

        // Gets the intent for the next started activity
        final Intent launchIntent = getStartedActivityIntent();
        // Verify the intent isn't null
        assertNotNull("Registration intent was null", launchIntent);
        // Verify the launchActivity was finished after button click
        assertTrue(isFinishCalled());
    }

    /**
     * Tests the password reminder activity has launched successfully
     */
    @MediumTest
    public void testPasswordActivityLaunchedWithIntent()
    {
        startActivity(intent, null, null);
        final TextView tvPasswordReset = (TextView) getActivity().findViewById(R.id.account_password_recovery);
        tvPasswordReset.performClick();

        // Gets the intent for the next started activity
        final Intent launchIntent = getStartedActivityIntent();
        // Verify the intent isn't null
        assertNotNull("Registration intent was null", launchIntent);
        // Verify the launchActivity was finished after button click
        assertTrue(isFinishCalled());
    }

    @MediumTest
    public void testCorrectLoginActivityLaunched() throws InterruptedException
    {
        startActivity(intent, null, null);
        final Button btnLogin = (Button) getActivity().findViewById(R.id.account_login);
        final TextView tvCreateAccount = (TextView) getActivity().findViewById(R.id.account_registration);
        final TextView tvPasswordReset = (TextView) getActivity().findViewById(R.id.account_password_recovery);

        tvCreateAccount.setText("natashawhitter@live.co.uk");
        tvPasswordReset.setText("Natasha789");
        btnLogin.performClick();

        final Intent launchIntent = getStartedActivityIntent();
        // Verify the intent isn't null
        assertNotNull("Logged in intent was null", launchIntent);
        // Verify the launchActivity was finished after button click
        assertTrue(isFinishCalled());
        assertEquals("natashawhitter@live.co.uk", User.getCurrentUser().getEmail());

    }

    @MediumTest
    public void testIncorrectLoginActivityLaunched() throws InterruptedException
    {
        startActivity(intent, null, null);
        final Button btnLogin = (Button) getActivity().findViewById(R.id.account_login);
        final TextView tvCreateAccount = (TextView) getActivity().findViewById(R.id.account_registration);
        final TextView tvPasswordReset = (TextView) getActivity().findViewById(R.id.account_password_recovery);

        tvCreateAccount.setText("sample@sample.co.uk");
        tvPasswordReset.setText("sample");
        btnLogin.performClick();

        final Intent launchIntent = getStartedActivityIntent();
        // Verify the intent isn't null
        assertNotNull("Login intent was null", launchIntent);
        // Verify the launchActivity was finished after button click
        assertTrue(isFinishCalled());
    }

    public void testLifecycleCreate()
    {
        startActivity(intent, null, null);
    }
}