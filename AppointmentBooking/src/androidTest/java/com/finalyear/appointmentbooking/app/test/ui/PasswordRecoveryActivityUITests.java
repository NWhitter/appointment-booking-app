package com.finalyear.appointmentbooking.app.test.ui;

import android.test.ActivityInstrumentationTestCase2;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.finalyear.appointmentbooking.app.R;
import com.finalyear.appointmentbooking.app.ui.PasswordRecoveryActivity;
import com.robotium.solo.Solo;
import com.squareup.spoon.Spoon;

/**
 * Created by Natasha Whitter on 04/05/2014.
 */
public class PasswordRecoveryActivityUITests extends ActivityInstrumentationTestCase2<PasswordRecoveryActivity>
{
    private PasswordRecoveryActivity activity;
    private Solo solo;

    private Button btnSubmit;
    private TextView cancelTv;
    private EditText etEmail;

    public PasswordRecoveryActivityUITests()
    {
        super(PasswordRecoveryActivity.class);
    }

    @Override
    protected void setUp() throws Exception
    {
        super.setUp();
        activity = getActivity();
        solo = new Solo(getInstrumentation(), activity);
        btnSubmit = (Button) activity.findViewById(R.id.password_recovery_submit);
        cancelTv = (TextView) activity.findViewById(R.id.password_recovery_cancel);
        etEmail = (EditText) activity.findViewById(R.id.password_recovery_email);
    }

    public void testRegisteredEmail()
    {
        Spoon.screenshot(activity, "initial_state");

        solo.enterText(etEmail, "natashawhitter@live.co.uk");
        assertEquals(true, (btnSubmit).isEnabled());

        solo.clickOnView(btnSubmit);
        solo.waitForText("Password recovery email has been sent");
        assertEquals(PasswordRecoveryActivity.class, getActivity().getClass());

        Spoon.screenshot(getActivity(), "review");
    }


    public void testUnregisteredEmail()
    {
        Spoon.screenshot(activity, "initial_state");

        solo.enterText(etEmail, "a@live.co.uk");
        assertEquals(true, (btnSubmit).isEnabled());

        solo.clickOnView(btnSubmit);
        solo.waitForText("no user found with email a@live.co.uk");
        assertEquals(PasswordRecoveryActivity.class, getActivity().getClass());

        Spoon.screenshot(getActivity(), "review");
    }

    public void testEmailValidation()
    {
        Spoon.screenshot(activity, "initial_state");

        solo.enterText(etEmail, "");
        assertEquals(true, (btnSubmit).isEnabled());
        solo.clickOnView(btnSubmit);
        solo.waitForText("Email is required");
        assertEquals(getActivity(), activity);

        solo.enterText(etEmail, "sample32");
        assertEquals(true, (btnSubmit).isEnabled());
        solo.clickOnView(btnSubmit);
        solo.waitForText("Email is incorrect");
        assertEquals(getActivity(), activity);

        Spoon.screenshot(activity, "review");
    }

    public void testResetNoInternet()
    {
        fail();
    }
}
