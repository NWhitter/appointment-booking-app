package com.finalyear.appointmentbooking.app.test.ui;

import android.test.ActivityInstrumentationTestCase2;
import com.finalyear.appointmentbooking.app.ui.BookingActivity;
import com.finalyear.appointmentbooking.app.ui.RegistrationActivity;
import com.robotium.solo.Solo;

/**
 * Created by Natasha Whitter on 04/05/2014.
 */
public class BookAppointmentActivityUITests extends ActivityInstrumentationTestCase2<BookingActivity>
{
    private BookingActivity activity;
    private Solo solo;

    public BookAppointmentActivityUITests()
    {
        super(BookingActivity.class);
    }

    @Override
    protected void setUp() throws Exception
    {
        super.setUp();
        activity = getActivity();
        solo = new Solo(getInstrumentation(), activity);
    }
}
