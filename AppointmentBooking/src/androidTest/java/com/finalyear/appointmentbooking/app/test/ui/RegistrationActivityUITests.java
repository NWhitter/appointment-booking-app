package com.finalyear.appointmentbooking.app.test.ui;

import android.test.ActivityInstrumentationTestCase2;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import com.finalyear.appointmentbooking.app.R;
import com.finalyear.appointmentbooking.app.data.Practice;
import com.finalyear.appointmentbooking.app.ui.LoginActivity;
import com.finalyear.appointmentbooking.app.ui.RegistrationActivity;
import com.finalyear.appointmentbooking.app.ui.RegistrationPracticeFragment;
import com.robotium.solo.Solo;

/**
 * Created by Natasha Whitter on 02/05/2014.
 */
public class RegistrationActivityUITests extends ActivityInstrumentationTestCase2<RegistrationActivity>
{
    private RegistrationActivity activity;
    private Solo solo;
    private Practice practice;
    private EditText tvFirstName;
    private EditText tvSurname;
    private EditText tvEmail;
    private EditText tvPassword;
    private EditText tvPasswordAgain;
    private Button btnBirthDate;
    private Button btnSignUp;
    private TextView tvSignIn;

    public RegistrationActivityUITests()
    {
        super(RegistrationActivity.class);
    }

    @Override
    protected void setUp() throws Exception
    {
        super.setUp();
        activity = getActivity();
        solo = new Solo(getInstrumentation(), activity);
        tvFirstName = (EditText) activity.findViewById(R.id.reg_first_name);
        tvSurname = (EditText) activity.findViewById(R.id.reg_surname);
        tvEmail = (EditText) activity.findViewById(R.id.reg_email);
        tvPassword = (EditText) activity.findViewById(R.id.reg_password);
        tvPasswordAgain = (EditText) activity.findViewById(R.id.reg_password_again);
        btnBirthDate = (Button) activity.findViewById(R.id.reg_birth_date);
        btnSignUp = (Button) activity.findViewById(R.id.reg_create_account);
        tvSignIn = (TextView) activity.findViewById(R.id.reg_sign_in);
    }

    public void testFirstNameValidation()
    {
        solo.clickInList(0);
        solo.clickInList(0);
        solo.enterText(tvFirstName, " ");
        assertEquals(true, (btnSignUp).isEnabled());
        solo.clickOnView(btnSignUp);
        assertTrue(solo.waitForText("First name is required"));

        solo.enterText(tvFirstName, "Nata8ha");
        assertEquals(true, (btnSignUp).isEnabled());
        solo.clickOnView(btnSignUp);
        assertTrue(solo.waitForText("First name is not valid"));
    }

    public void testSurnameValidation()
    {
        solo.clickInList(0);
        solo.clickInList(0);
        solo.enterText(tvSurname, " ");
        assertEquals(true, (btnSignUp).isEnabled());
        solo.clickOnView(btnSignUp);
        solo.enterText(tvSurname, "Whi8tter");
        assertEquals(true, (btnSignUp).isEnabled());
        solo.clickOnView(btnSignUp);
    }

    /*public void testBirthDateValidation()
    {
        fail();
    }

    public void testPasswordValidation()
    {
        fail();
    }

    public void testEmailValidation()
    {
        fail();
    }

    public void testCorrectRegistration()
    {
        fail();
    }

    public void testAlreadyRegistration()
    {
        fail();
    }

    public void testRegisterNoInternet()
    {
        fail();
    }*/
}
