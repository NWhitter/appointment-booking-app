package com.finalyear.appointmentbooking.app.test.ui;

import android.test.ActivityInstrumentationTestCase2;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.finalyear.appointmentbooking.app.R;
import com.finalyear.appointmentbooking.app.data.User;
import com.finalyear.appointmentbooking.app.nav.NavigationDrawer;
import com.finalyear.appointmentbooking.app.ui.LoginActivity;
import com.finalyear.appointmentbooking.app.ui.PasswordRecoveryActivity;
import com.finalyear.appointmentbooking.app.ui.RegistrationActivity;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.SignUpCallback;
import com.robotium.solo.Solo;
import com.squareup.spoon.Spoon;

import java.util.Calendar;

// import static org.fest.assertions.api.ANDROID.assertThat;

public class AccountLoginActivityUITests extends ActivityInstrumentationTestCase2<LoginActivity>
{
    private LoginActivity activity;
    private Solo solo;

    private Button btnSignIn;
    private EditText etEmail;
    private EditText etPassword;
    private TextView tvPassword;
    private TextView tvSignUp;

    public AccountLoginActivityUITests()
    {
        super(LoginActivity.class);
    }

    protected void setUp() throws Exception
    {
        super.setUp();
        activity = getActivity();
        solo = new Solo(getInstrumentation(), activity);
        btnSignIn = (Button) activity.findViewById(R.id.account_login);
        etEmail = (EditText) activity.findViewById(R.id.account_email);
        etPassword = (EditText) activity.findViewById(R.id.account_password);
        tvPassword = (TextView) activity.findViewById(R.id.account_password_recovery);
        tvSignUp = (TextView) activity.findViewById(R.id.account_registration);
    }

    public void testUnregisteredAccount()
    {
        Spoon.screenshot(activity, "initial_state");

        solo.enterText(etEmail, "sample@sample.co.uk");
        solo.enterText(etPassword, "Sample7890");
        assertEquals(true, (btnSignIn).isEnabled());

        solo.clickOnView(btnSignIn);
        solo.waitForText("invalid login credentials");
        assertEquals(getActivity(), activity);

        Spoon.screenshot(activity, "review");
    }

    public void testRegisteredAccount()
    {
        Spoon.screenshot(activity, "initial_state");

        solo.enterText(etEmail, "natashawhitter@live.co.uk");
        solo.enterText(etPassword, "Natasha789");
        assertEquals(true, (btnSignIn).isEnabled());

        solo.clickOnView(btnSignIn);
        solo.waitForActivity(NavigationDrawer.class);
        assertEquals(NavigationDrawer.class, getActivity().getClass());

        Spoon.screenshot(getActivity(), "review");
    }

    public void testLoginUserSpeed()
    {
        Spoon.screenshot(activity, "initial_state");

        solo.enterText(etEmail, "natashawhitter@live.co.uk");
        solo.enterText(etPassword, "Natasha789");
        assertEquals(true, (btnSignIn).isEnabled());
        Long beforeLogin = System.currentTimeMillis();

        solo.clickOnView(btnSignIn);
        solo.waitForActivity(NavigationDrawer.class);

        Long afterLogin = System.currentTimeMillis();

        assertEquals(NavigationDrawer.class, getActivity().getClass()); // REDO
        assertTrue(5000 > (afterLogin - beforeLogin));

        Spoon.screenshot(getActivity(), "review");
    }

    public void testEmailValidation()
    {
        Spoon.screenshot(activity, "initial_state");

        solo.enterText(etEmail, "");
        solo.enterText(etPassword, "Natasha789");
        assertEquals(true, (btnSignIn).isEnabled());
        solo.clickOnView(btnSignIn);
        solo.waitForText("Email is required");
        assertEquals(getActivity(), activity);

        solo.enterText(etEmail, "sample32");
        assertEquals(true, (btnSignIn).isEnabled());
        solo.clickOnView(btnSignIn);
        solo.waitForText("Email is incorrect");
        assertEquals(getActivity(), activity);

        Spoon.screenshot(activity, "review");
    }

    public void testPasswordValidation()
    {
        Spoon.screenshot(activity, "initial_state");

        solo.enterText(etEmail, "natashawhitter@live.co.uk");
        solo.enterText(etPassword, "");
        assertEquals(true, (btnSignIn).isEnabled());
        solo.clickOnView(btnSignIn);
        solo.waitForText("Password is required");
        assertEquals(getActivity(), activity);

        Spoon.screenshot(activity, "review");
    }

    public void testPasswordScreenOnClick()
    {
        Spoon.screenshot(activity, "initial_state");

        solo.clickOnView(tvPassword);
        solo.waitForActivity(PasswordRecoveryActivity.class);
        assertEquals(PasswordRecoveryActivity.class, getActivity().getClass());

        Spoon.screenshot(activity, "review");
    }

    public void testSignUpScreenOnClick()
    {
        Spoon.screenshot(activity, "initial_state");

        solo.clickOnView(tvSignUp);
        solo.waitForActivity(RegistrationActivity.class);
        assertEquals(RegistrationActivity.class, getActivity().getClass());

        Spoon.screenshot(activity, "review");
    }
}
