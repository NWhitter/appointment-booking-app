package com.finalyear.appointmentbooking.app.test;

import android.app.Application;
import android.app.KeyguardManager;
import android.content.Context;
import android.os.PowerManager;
import android.test.InstrumentationTestRunner;

/**
 * ${@link "https://github.com/square/spoon/blob/master/spoon-sample/tests/src/main/java/com/example/spoon/ordering/tests/SpoonInstrumentationTestRunner.java"}
 *
 * @author Natasha Whitter <Natasha Whitter>
 * @version 1.0
 * @since 11/04/2014
 */
public class SpoonInstrumentationTestRunner extends InstrumentationTestRunner
{
    @Override
    public void onStart()
    {
        super.onStart();
        runOnMainSync(new Runnable()
        {
            @Override
            public void run()
            {
                Application app = (Application) getTargetContext().getApplicationContext();
                String simpleName = SpoonInstrumentationTestRunner.class.getSimpleName();

                // Unlock the device so input keystrokes can be used in tests
                ((KeyguardManager) app.getSystemService(Context.POWER_SERVICE)).newKeyguardLock(simpleName).disableKeyguard();
                ((PowerManager) app.getSystemService(Context.POWER_SERVICE)).newWakeLock(PowerManager.FULL_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP | PowerManager.ON_AFTER_RELEASE, simpleName).acquire();
            }
        });
    }
}
