package com.finalyear.appointmentbooking.app.data;

import com.parse.ParseClassName;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.Date;

/**
 * This AppointmentDate class is an extension of ParseObject to access AppointmentDate objects
 *
 * @author Natasha Whitter <Natasha Whitter>
 * @version 1.0
 * @see com.parse.ParseObject
 * @since 08/042/2014
 */
@ParseClassName("AppointmentDate")
public class AppointmentDate extends ParseObject
{
    /**
     * Empty constructor needed for ParseObject class
     */
    public AppointmentDate()
    {
    }

    /**
     * Gets the class which the query will use to access AppointmentDate data.
     *
     * @return query to fetch AppointmentDate objects
     * @see com.parse.ParseQuery
     */
    public static ParseQuery<AppointmentDate> getQuery()
    {
        return ParseQuery.getQuery(AppointmentDate.class);
    }

    /**
     * Gets the date of this AppointmentDate
     *
     * @return this AppointmentDate's date
     */
    public Date getAppointmentDate()
    {
        return super.getDate("appointmentDate");
    }

    /**
     * Changes the date of this AppointmentDate
     *
     * @param date This AppointmentDate's new date
     */
    public void setAppointmentDate(Date date)
    {
        super.put("appointmentDate", date);
    }

}
