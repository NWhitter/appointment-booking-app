package com.finalyear.appointmentbooking.app.ui;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.finalyear.appointmentbooking.app.R;
import com.finalyear.appointmentbooking.app.Validation;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.RequestPasswordResetCallback;

/**
 * @author Natasha Whitter <Natasha Whitter>
 * @version 1.0
 * @since 19/04/2014
 */
public class PasswordRecoveryActivity extends Activity
{
    final View.OnClickListener btnOnClickListener = new View.OnClickListener()
    {
        @Override
        public void onClick(View v)
        {
            switch (v.getId())
            {
                case R.id.password_recovery_submit:
                {
                    submitEmail();
                    break;
                }
                case R.id.password_recovery_cancel:
                    Intent i = new Intent(getApplicationContext(), LoginActivity.class);
                    startActivity(i);
                    finish();
                    break;
            }
        }
    };
    private Button btnSubmit;
    private TextView cancelTv;
    private EditText etEmail;

    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_password_recovery);

        btnSubmit = (Button) findViewById(R.id.password_recovery_submit);
        cancelTv = (TextView) findViewById(R.id.password_recovery_cancel);
        etEmail = (EditText) findViewById(R.id.password_recovery_email);

        etEmail.setText(getIntent().getStringExtra("email"));
        btnSubmit.setOnClickListener(btnOnClickListener);
        cancelTv.setOnClickListener(btnOnClickListener);
    }

    /**
     * If the email supplied is correct, then the email is sent to the Parse service. If a ParseException occurs,
     * then toast the message to the screen
     */
    public void submitEmail()
    {
        if (Validation.isEmpty(etEmail))
        {
            Toast.makeText(this, getString(R.string.error_email_required), Toast.LENGTH_SHORT).show();
        }
        else if (!Validation.isValidEmail(etEmail))
        {
            Toast.makeText(this, getString(R.string.error_invalid_email), Toast.LENGTH_SHORT).show();
        }
        else
        {
            final ProgressDialog recoveryDialog = new ProgressDialog(PasswordRecoveryActivity.this);
            recoveryDialog.setIndeterminate(true);
            recoveryDialog.setMessage("Sending password reset email. Please wait.");
            recoveryDialog.show();

            ParseUser.requestPasswordResetInBackground(etEmail.getText().toString().toLowerCase(), new RequestPasswordResetCallback()
            {
                @Override
                public void done(ParseException e)
                {
                    recoveryDialog.dismiss();
                    if (e == null)
                    {
                        Toast.makeText(PasswordRecoveryActivity.this, getString(R.string.prompt_email_sent), Toast.LENGTH_LONG).show();
                    } else
                    {
                        Toast.makeText(PasswordRecoveryActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                        Intent i = new Intent(PasswordRecoveryActivity.this, LoginActivity.class);
                        startActivity(i);
                        finish();
                    }
                }
            });
        }
    }
}
