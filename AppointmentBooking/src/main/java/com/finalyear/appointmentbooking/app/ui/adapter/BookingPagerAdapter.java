package com.finalyear.appointmentbooking.app.ui.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import com.finalyear.appointmentbooking.app.ui.BookingDetailsFragment;
import com.finalyear.appointmentbooking.app.ui.BookingDoctorFragment;
import com.finalyear.appointmentbooking.app.ui.BookingSlotFragment;

/**
 * @author Natasha Whitter <Natasha Whitter>
 * @version 1.0
 * @since 17/04/2014
 */
public class BookingPagerAdapter extends FragmentPagerAdapter
{
    final int PAGE_COUNT = 3;

    public BookingPagerAdapter(FragmentManager fm)
    {
        super(fm);
    }

    @Override
    public Fragment getItem(int position)
    {
        switch (position)
        {
            case 0:
                return new BookingDoctorFragment();
            case 1:
                return new BookingSlotFragment();
            case 2:
                return new BookingDetailsFragment();
            default:
                return null;
        }
    }

    @Override
    public int getCount()
    {
        return PAGE_COUNT;
    }

    @Override
    public CharSequence getPageTitle(int position)
    {
        return "Page #" + position + 1;
    }
}
