package com.finalyear.appointmentbooking.app.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import com.finalyear.appointmentbooking.app.R;
import com.finalyear.appointmentbooking.app.data.Appointment;
import com.finalyear.appointmentbooking.app.data.Doctor;
import com.finalyear.appointmentbooking.app.ui.adapter.BookingPagerAdapter;
import com.finalyear.appointmentbooking.app.ui.interfaces.IBookingClickListener;
import com.finalyear.appointmentbooking.app.view.CustomViewPager;

/**
 * @author Natasha Whitter <Natasha Whitter>
 * @version 1.0
 * @since 17/04/2014
 */
public class BookingActivity extends FragmentActivity implements IBookingClickListener
{
    private CustomViewPager pager;
    private BookingPagerAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.viewpager);

        pager = (CustomViewPager) findViewById(R.id.viewpager);

        if (getActionBar() != null)
        {
            getActionBar().setDisplayHomeAsUpEnabled(true);
            getActionBar().setTitle("Book Appointment");
        }

        adapter = new BookingPagerAdapter(getSupportFragmentManager());
        pager.setAdapter(adapter);
    }

    @Override
    public void onBackPressed()
    {
        if (pager.getCurrentItem() == 0)
        {
            super.onBackPressed();
        } else
        {
            pager.setCurrentItem(pager.getCurrentItem() - 1);
        }
    }

    @Override
    public void previousFragment()
    {
        if (pager.getCurrentItem() > 0)
        {
            pager.setCurrentItem(pager.getCurrentItem() - 1);
        }
    }

    @Override
    public void nextFragment()
    {
        if (pager.getCurrentItem() + 1 < adapter.getCount())
        {
            pager.setCurrentItem(pager.getCurrentItem() + 1);
        }
    }

    @Override
    public void onListClick(Doctor doctor)
    {
        BookingSlotFragment object = (BookingSlotFragment) getSupportFragmentManager().getFragments().get(1);
        object.setDoctor(doctor);
    }

    @Override
    public void onListClick(Appointment appointment)
    {
        BookingDetailsFragment object = (BookingDetailsFragment) getSupportFragmentManager().getFragments().get(2);
        object.setAppointment(appointment);
    }

    public void fragmentResults(int result)
    {
        Intent intent = new Intent();
        if (result == RESULT_OK)
        {
            setResult(RESULT_OK, intent);
        } else
        {
            setResult(RESULT_CANCELED, intent);
        }
        finish();
    }

}
