package com.finalyear.appointmentbooking.app.ui;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;
import com.finalyear.appointmentbooking.app.ApplicationConstants;
import com.finalyear.appointmentbooking.app.R;
import com.finalyear.appointmentbooking.app.data.Doctor;
import com.finalyear.appointmentbooking.app.ui.adapter.DoctorsAdapter;
import com.finalyear.appointmentbooking.app.ui.interfaces.IBookingClickListener;

/**
 * @author Natasha Whitter <Natasha Whitter>
 * @version 1.0
 * @since 17/04/2014
 */
public class BookingDoctorFragment extends Fragment
{
    IBookingClickListener listener;

    private ListView lvDoctor;
    private DoctorsAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_booking_select_doctor, container, false);

        lvDoctor = (ListView) view.findViewById(R.id.lvDoctors);
        adapter = new DoctorsAdapter(getActivity());
        adapter.setTextKey("surname");
        lvDoctor.setAdapter(adapter);

        lvDoctor.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                if (ApplicationConstants.isNetworkConnected(getActivity()))
                {
                    Doctor doctor = adapter.getItem(position);
                    listener.onListClick(doctor);
                    listener.nextFragment();
                } else
                {
                    Toast.makeText(getActivity(), "Please connect to the internet to carry booking appointments", Toast.LENGTH_SHORT).show();
                }
            }
        });

        return view;
    }

    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);

        try
        {
            listener = (IBookingClickListener) getActivity();
        } catch (ClassCastException e)
        {
            throw new ClassCastException(activity.toString() + " must implement IBookingClickListener");
        }
    }
}
