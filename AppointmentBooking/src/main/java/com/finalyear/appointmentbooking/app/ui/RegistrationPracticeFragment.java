package com.finalyear.appointmentbooking.app.ui;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import com.finalyear.appointmentbooking.app.data.Practice;
import com.finalyear.appointmentbooking.app.ui.adapter.PracticeAdapter;
import com.finalyear.appointmentbooking.app.ui.interfaces.IRegisterClickListener;
import com.parse.ParseQueryAdapter;

/**
 * @author Natasha Whitter <Natasha Whitter>
 * @version 1.0
 * @since 25/04/2014
 */
public class RegistrationPracticeFragment extends ListFragment implements AdapterView.OnItemClickListener
{
    IRegisterClickListener listener;

    private String TAG = "RegistrationPracticeFragment";
    private ParseQueryAdapter<Practice> adapter;
    private Practice practice;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        adapter = new PracticeAdapter(getActivity());
        adapter.setTextKey("name");
        setListAdapter(adapter);
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onResume()
    {
        super.onResume();
        updatePracticeList();
    }

    private void updatePracticeList()
    {
        adapter.loadObjects();
        setListAdapter(adapter);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id)
    {
        Practice practice1 = adapter.getItem(position);
        listener.onListClick(practice1);
        listener.nextFragment();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);
        getListView().setOnItemClickListener(this);
    }

    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(getActivity());

        try
        {
            listener = (IRegisterClickListener) getActivity();
        } catch (ClassCastException e)
        {
            throw new ClassCastException(activity.toString() + " must implement IRegisterClickListener");
        }
    }
}