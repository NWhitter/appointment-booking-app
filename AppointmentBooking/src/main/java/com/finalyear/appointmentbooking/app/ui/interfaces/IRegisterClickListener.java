package com.finalyear.appointmentbooking.app.ui.interfaces;

import com.finalyear.appointmentbooking.app.data.Practice;

/**
 * @author Natasha Whitter <Natasha Whitter>
 * @version 1.0
 * @since 25/04/2014
 */
public interface IRegisterClickListener extends IClickListener
{
    public void onListClick(Practice practice);
}
