package com.finalyear.appointmentbooking.app.data;

import com.parse.ParseClassName;
import com.parse.ParseObject;
import com.parse.ParseQuery;

/**
 * This Address class is an extension of ParseObject to access Address objects
 *
 * @author Natasha Whitter <Natasha Whitter>
 * @version 1.0
 * @see com.parse.ParseObject
 * @since 08/04/2014
 */

@ParseClassName("Address")
public class Address extends ParseObject
{
    /**
     * Empty constructor needed for ParseObject class
     */
    public Address()
    {
    }

    /**
     * Gets the class which the query will use to access Address data.
     *
     * @return query to fetch Address objects
     * @see com.parse.ParseQuery
     */
    public static ParseQuery<Address> getQuery()
    {
        return ParseQuery.getQuery(Address.class);
    }

    /**
     * Gets the street name of this Address.
     *
     * @return this Address' street
     */
    public String getStreet()
    {
        return super.getString("street");
    }

    /**
     * Changes the street name of this Address.
     *
     * @param street This Address' new street
     */
    public void setStreet(String street)
    {
        super.put("street", street);
    }

    /**
     * Gets the city name of this Address.
     *
     * @return this Address' city
     */
    public String getCity()
    {
        return super.getString("city");
    }

    /**
     * Changes the city name of this Address.
     *
     * @param city This Address' new city
     */
    public void setCity(String city)
    {
        super.put("city", city);
    }

    /**
     * Gets the county name of this Address.
     *
     * @return this Address' county
     */
    public String getCounty()
    {
        return super.getString("county");
    }

    /**
     * Changes the county name of this Address.
     *
     * @param county This Address' new county
     */
    public void setCounty(String county)
    {
        super.put("county", county);
    }

    /**
     * Gets the postcode name of this Address.
     *
     * @return this Address' postcode
     */
    public String getPostcode()
    {
        return super.getString("postcode");
    }

    /**
     * Changes the postcode of this Address.
     *
     * @param postcode This Address' new postcode
     */
    public void setPostcode(String postcode)
    {
        super.put("postcode", postcode);
    }

    /**
     * Gets the country name of this Address.
     *
     * @return this Address' country
     */
    public String getCountry()
    {
        return super.getString("country");
    }

    /**
     * Changes the country of this Address.
     *
     * @param country This Address' new country
     */
    public void setCountry(String country)
    {
        super.put("country", country);
    }
}
