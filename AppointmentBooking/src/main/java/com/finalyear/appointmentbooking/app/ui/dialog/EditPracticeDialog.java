package com.finalyear.appointmentbooking.app.ui.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.widget.Spinner;
import com.finalyear.appointmentbooking.app.R;
import com.finalyear.appointmentbooking.app.data.Practice;
import com.finalyear.appointmentbooking.app.data.User;
import com.finalyear.appointmentbooking.app.ui.adapter.PracticeAdapter;
import com.parse.ParseException;
import com.parse.ParseQueryAdapter;
import com.parse.SaveCallback;

/**
 * @author Natasha Whitter <Natasha Whitter>
 * @version 1.0
 * @since 27/04/2014
 */
public class EditPracticeDialog extends DialogFragment
{
    private ParseQueryAdapter<Practice> adapter;
    private Spinner sPractice;
    private Practice practice;
    private User user;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState)
    {
        AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
        View view = getActivity().getLayoutInflater().inflate(R.layout.fragment_dialog_edit_practice, null);
        sPractice = (Spinner) view.findViewById(R.id.practice_list);
        user = (User) User.getCurrentUser();

        adapter = new PracticeAdapter(getActivity());
        adapter.setTextKey("name");
        sPractice.setAdapter(adapter);

        dialog.setView(view);

        dialog.setTitle("Change Practice");
        dialog.setPositiveButton("OK", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int position)
            {
                savePractice();
                dialog.dismiss();
            }
        });

        dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int position)
            {
                getTargetFragment().onActivityResult(getTargetRequestCode(), getActivity().RESULT_CANCELED, null);
                dialog.cancel();
            }
        });

        return dialog.create();
    }

    public void savePractice()
    {
        user.setPractice((Practice) sPractice.getSelectedItem());
        final ProgressDialog dialog = new ProgressDialog(getActivity());
        dialog.setIndeterminate(true);
        dialog.setMessage("Changing GP Practice. Please wait.");
        dialog.show();
        user.saveInBackground(new SaveCallback()
        {
            @Override
            public void done(ParseException e)
            {
                dialog.dismiss();
                if (e == null)
                {
                    getTargetFragment().onActivityResult(getTargetRequestCode(), getActivity().RESULT_OK, null);
                }
            }
        });
    }

    @Override
    public void onResume()
    {
        super.onResume();
        updatePracticeList();
    }

    private void updatePracticeList()
    {
        adapter.loadObjects();
        sPractice.setAdapter(adapter);
    }
}
