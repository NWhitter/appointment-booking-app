package com.finalyear.appointmentbooking.app.view;

// http://stackoverflow.com/questions/9650265/how-do-disable-paging-by-swiping-with-finger-in-viewpager-but-still-be-able-to-s

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

public class CustomViewPager extends ViewPager
{
    private boolean isSwipeable = false;

    public CustomViewPager(Context context)
    {
        super(context);
        this.isSwipeable = false;
    }

    public CustomViewPager(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        this.isSwipeable = false;
    }

    public CustomViewPager(Context context, AttributeSet attrs, boolean swipeable)
    {
        super(context, attrs);
        this.isSwipeable = swipeable;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event)
    {
        if (this.isSwipeable)
        {
            return super.onInterceptTouchEvent(event);
        }
        return false;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event)
    {
        if (this.isSwipeable)
        {
            return super.onTouchEvent(event);
        }
        return false;
    }

    public void setSwipeable(boolean swipeable)
    {
        this.isSwipeable = swipeable;
    }

}
