package com.finalyear.appointmentbooking.app;

import android.text.TextUtils;
import android.widget.EditText;

import java.util.Calendar;
import java.util.Locale;

// referenced from: http://tausiq.wordpress.com/2013/01/19/android-input-field-validation/

/**
 * Methods for validating information
 *
 * @author Natasha Whitter
 * @version 1.0
 * @since 21/02/2014
 */
public class Validation
{
    private static final String PASSWORD_PATTERN = "((?=.*\\d)(?=.*[a-z]).{6,20})";
    private static final String TAG = "Validation";
    public static String User = "User";

    /**
     * Checks if the email correctly matches the email pattern.
     * Returns true if the email matches the pattern, otherwise the method returns false if the email doesn't match the pattern
     *
     * @param etEmail The email to validate
     * @return if the email matches the pattern
     */
    public static boolean isValidEmail(EditText etEmail)
    {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(etEmail.getText().toString().trim()).matches();
    }

    /**
     * Checks if the password correctly matches the password pattern.
     * Returns true if the password matches the pattern, otherwise the method returns false if the password doesn't match the pattern
     *
     * @param etPassword The password to validate
     * @return if the password matches the pattern
     */
    public static boolean isValidPassword(EditText etPassword)
    {
        return etPassword.getText().toString().matches(PASSWORD_PATTERN);
    }

    // Check if passwords are both matching
    public static boolean isPasswordsMatching(EditText etPassword, EditText etPassword2)
    {
        return (etPassword.getText().toString().equals(etPassword2.getText().toString()));
    }

    // Check if input field has only digits
    public static boolean hasOnlyDigits(EditText etText)
    {
        return (TextUtils.isDigitsOnly(etText.getText().toString().trim()));
    }

    public static boolean hasOnlyLetters(EditText etText)
    {
        return (etText.getText().toString().trim().matches("[a-zA-Z]+"));
    }

    // Check if input field is empty or not
    public static boolean isEmpty(EditText etText)
    {
        return (TextUtils.isEmpty(etText.getText().toString().trim()));
    }

    // Check if the user's age is above 18
    public static boolean is18(Calendar birthDate)
    {
        Calendar currentDate = Calendar.getInstance(Locale.getDefault());

        int age = currentDate.get(Calendar.YEAR) - birthDate.get(Calendar.YEAR);
        if (currentDate.get(Calendar.MONTH) < birthDate.get(Calendar.MONTH))
        {
            age--;
        } else if (currentDate.get(Calendar.MONTH) == birthDate.get(Calendar.MONTH)
                && currentDate.get(Calendar.DAY_OF_MONTH) < birthDate.get(Calendar.DAY_OF_MONTH))
        {
            age--;
        }

        return (age >= 18);
    }
}
