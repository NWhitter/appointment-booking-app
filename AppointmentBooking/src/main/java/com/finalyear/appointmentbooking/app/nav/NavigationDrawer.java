package com.finalyear.appointmentbooking.app.nav;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ListView;
import com.finalyear.appointmentbooking.app.R;
import com.finalyear.appointmentbooking.app.ui.*;
import com.parse.ParseAnalytics;

/**
 * @author Natasha Whitter <Natasha Whitter>
 * @version 1.0
 * @since 21/02/2014
 */

public class NavigationDrawer extends FragmentActivity
{
    private FragmentNavigationDrawer drawer;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        // Start tracking analytics data
        ParseAnalytics.trackAppOpened(getIntent());

        setContentView(R.layout.drawer_navigation);

        drawer = (FragmentNavigationDrawer) findViewById(R.id.drawer_layout);
        drawer.setupDrawerConfiguration((ListView) findViewById(R.id.drawer_list),  R.id.drawer_frame);

        // Fragments to be added in the navigation drawer
        drawer.addNavItem("Appointments", R.drawable.ic_action_app, "Appointments", AppointmentsFragment.class);
        drawer.addNavItem("Past Appointments", R.drawable.ic_action_pastapp,"Past Appointments", PastAppointmentsFragment.class);
        drawer.addNavItem("Practice Details", R.drawable.ic_action_home,"Practice Details", PracticeDetailsFragment.class);
        drawer.addNavItem("Account Settings", R.drawable.ic_action_settings,"Account Settings", AccountDetailsFragment.class);
        drawer.addNavItem("About", R.drawable.ic_action_account, "About", AboutFragment.class);
        drawer.addNavItem("Logout", R.drawable.ic_action_logout, "Logout", LogoutFragment.class);

        if (savedInstanceState == null)
        {
            drawer.selectDrawerItem(0);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // The action bar home/up action should open or close the drawer.
        // ActionBarDrawerToggle will take care of this.
        if (drawer.getDrawerToggle().onOptionsItemSelected(item))
        {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState)
    {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        drawer.getDrawerToggle().syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig)
    {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggles
        drawer.getDrawerToggle().onConfigurationChanged(newConfig);
    }

    /**
     * Checks if the user wants to close the application.
     * If yes, then the application will close.
     * If no, then the application will stay open.
     */
    @Override
    public void onBackPressed()
    {
        new AlertDialog.Builder(this).setTitle("Leaving " + getResources().getString(R.string.app_name))
                .setMessage("Are you sure you would like to leave?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        // To access the super, I have to access the class and call OnBackPressed
                        NavigationDrawer.super.onBackPressed();
                    }
                })
                // Button doesn't need to do anything
                .setNegativeButton(android.R.string.no, null)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }
}
