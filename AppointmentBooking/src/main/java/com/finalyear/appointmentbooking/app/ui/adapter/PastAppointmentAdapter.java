package com.finalyear.appointmentbooking.app.ui.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.finalyear.appointmentbooking.app.ApplicationConstants;
import com.finalyear.appointmentbooking.app.R;
import com.finalyear.appointmentbooking.app.data.Appointment;
import com.parse.ParseQuery;
import com.parse.ParseQueryAdapter;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.TimeZone;

/**
 * @author Natasha Whitter <Natasha Whitter>
 * @version 1.0
 * @since 22/04/2014
 */
public class PastAppointmentAdapter extends ParseQueryAdapter<Appointment>
{

    public PastAppointmentAdapter(Context context)
    {
        super(context, new QueryFactory<Appointment>()
        {
            @Override
            public ParseQuery<Appointment> create()
            {
                ParseQuery<Appointment> query = ParseQuery.getQuery("Appointment");
                query.setCachePolicy(ParseQuery.CachePolicy.NETWORK_ELSE_CACHE);
                Calendar today = Calendar.getInstance(TimeZone.getDefault());
                today.set(Calendar.HOUR_OF_DAY, 0);
                today.set(Calendar.SECOND, 0);
                today.set(Calendar.MINUTE, 0);
                today.set(Calendar.MILLISECOND, 0);
                query.whereLessThan("appointmentDate", today.getTime());
                query.include("doctor");
                query.include("slot");
                query.addAscendingOrder("appointmentDate");

                return query;
            }
        });
    }

    @Override
    public View getItemView(final Appointment object, View v, ViewGroup parent)
    {
        if (v == null)
        {
            v = android.view.View.inflate(getContext(), R.layout.item_list_appointments, null);
        }

        super.getItemView(object, v, parent);

        TextView tvDate = (TextView) v.findViewById(R.id.appointment_date);
        TextView tvDoctor = (TextView) v.findViewById(R.id.doctor_name);
        v.findViewById(R.id.cancel_appointment).setVisibility(View.GONE);

        SimpleDateFormat hourMinute = new SimpleDateFormat(ApplicationConstants.DEFAULT_TIME_12HR);
        SimpleDateFormat date = new SimpleDateFormat(ApplicationConstants.DEFAULT_DATE);

        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(object.getSlot().getStartTime());

        String appointmentDate = date.format(object.getAppointmentDate());
        String appointmentTime = hourMinute.format(cal.getTime());
        tvDate.setText(appointmentDate + " " + appointmentTime);
        tvDoctor.setText(object.getDoctor().toString() + " - Room: " + object.getDoctor().getOffice());

        return v;
    }
}
