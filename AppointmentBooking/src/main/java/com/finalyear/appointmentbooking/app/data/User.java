package com.finalyear.appointmentbooking.app.data;

import com.finalyear.appointmentbooking.app.data.interfaces.IPerson;
import com.parse.ParseObject;
import com.parse.ParseUser;

import java.util.Date;

/**
 * This User class is an extension of ParseUser which has extra details
 *
 * @author Natasha Whitter <Natasha Whitter>
 * @version 1.0
 * @see com.parse.ParseUser
 * @since 08/04/2014
 */
public class User extends ParseUser implements IPerson
{
    /**
     * Empty constructor needed for ParseUser class
     */
    public User()
    {
    }

    /**
     * Gets the title of this User.
     *
     * @return this User's title
     * @see com.finalyear.appointmentbooking.app.data.interfaces.IPerson#getTitle()
     */
    @Override
    public String getTitle()
    {
        return super.getString("title");
    }

    /**
     * Changes the title of this User.
     *
     * @param title This User's new title
     * @see com.finalyear.appointmentbooking.app.data.interfaces.IPerson#setTitle(String)
     */
    @Override
    public void setTitle(String title)
    {
        super.put("title", title);
    }

    /**
     * Gets the first name of this User.
     *
     * @return this User's first name
     * @see com.finalyear.appointmentbooking.app.data.interfaces.IPerson#getFirstName()
     */
    @Override
    public String getFirstName()
    {
        return super.getString("firstName");
    }

    /**
     * Changes the first name of this User.
     *
     * @param firstName This User's new first name
     * @see com.finalyear.appointmentbooking.app.data.interfaces.IPerson#setFirstName(String)
     */
    @Override
    public void setFirstName(String firstName)
    {
        super.put("firstName", firstName);
    }

    /**
     * Gets the surname of this User.
     *
     * @return this User's surname
     * @see com.finalyear.appointmentbooking.app.data.interfaces.IPerson#getSurname()
     */
    @Override
    public String getSurname()
    {
        return super.getString("surname");
    }

    /**
     * Changes the surname of this User.
     *
     * @param surname This User's new surname
     * @see com.finalyear.appointmentbooking.app.data.interfaces.IPerson#setSurname(String)
     */
    @Override
    public void setSurname(String surname)
    {
        super.put("surname", surname);
    }

    /**
     * Gets the date of birth of this User.
     *
     * @return this User's date of birth
     * @see com.finalyear.appointmentbooking.app.data.interfaces.IPerson#getDateOfBirth()
     */
    @Override
    public Date getDateOfBirth()
    {
        return super.getDate("dateOfBirth");
    }

    /**
     * Changes the date of birth of this User.
     *
     * @param birthDate This User's new date of birth
     * @see com.finalyear.appointmentbooking.app.data.interfaces.IPerson#setDateOfBirth(java.util.Date)
     */
    @Override
    public void setDateOfBirth(Date birthDate)
    {
        super.put("dateOfBirth", birthDate);
    }

    /**
     * Gets the gender of this User.
     *
     * @return this User's gender
     * @see com.finalyear.appointmentbooking.app.data.interfaces.IPerson#getGender()
     */
    @Override
    public String getGender()
    {
        return super.getString("gender");
    }

    /**
     * Changes the gender of this User.
     *
     * @param gender This User's new gender
     * @see com.finalyear.appointmentbooking.app.data.interfaces.IPerson#setGender(String)
     */
    @Override
    public void setGender(String gender)
    {
        super.put("gender", gender);
    }

    /**
     * Gets the practice assigned to this User.
     *
     * @return this User's assigned doctor
     * @see com.finalyear.appointmentbooking.app.data.interfaces.IPerson#getPractice()
     */
    @Override
    public ParseObject getPractice()
    {
        return super.getParseObject("practice");
    }

    /**
     * Changes the practice assigned to this User
     *
     * @param GPPractice This User's new GP Practice
     * @see com.finalyear.appointmentbooking.app.data.interfaces.IPerson#setPractice(com.parse.ParseObject)
     */
    @Override
    public void setPractice(ParseObject GPPractice)
    {
        super.put("practice", GPPractice);
    }
}
