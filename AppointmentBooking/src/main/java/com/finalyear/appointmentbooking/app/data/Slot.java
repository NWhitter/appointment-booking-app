package com.finalyear.appointmentbooking.app.data;

import com.parse.ParseClassName;
import com.parse.ParseObject;
import com.parse.ParseQuery;

/**
 * This Slot class is an extension of ParseObject to access Slot objects
 *
 * @author Natasha Whitter <Natasha Whitter>
 * @version 1.0
 * @see com.parse.ParseObject
 * @since 08/04/2014
 */

@ParseClassName("Slot")
public class Slot extends ParseObject
{
    /**
     * Empty constructor needed for ParseObject class
     */
    public Slot()
    {
    }

    /**
     * Gets the class which the query will use to access Slot data.
     *
     * @return query to fetch Slot objects
     * @see com.parse.ParseQuery
     */
    public static ParseQuery<Slot> getQuery()
    {
        return ParseQuery.getQuery(Slot.class);
    }

    /**
     * Gets the start time of this Slot.
     *
     * @return this Slot's start time
     */
    public Long getStartTime()
    {
        return super.getLong("startTime");
    }

    /**
     * Changes the start time of this Slot.
     *
     * @param time This Slot's new start time
     */
    public void setStartTime(Long time)
    {
        super.put("startTime", time);
    }

    /**
     * Gets the finish time of this Slot.
     *
     * @return this Slot's finish time
     */
    public Long getFinishTime()
    {
        return super.getLong("finishTime");
    }

    /**
     * Changes the finish time of this Slot.
     *
     * @param time This Slot's new finish time
     */
    public void setFinishTime(Long time)
    {
        super.put("finishTime", time);
    }
}
