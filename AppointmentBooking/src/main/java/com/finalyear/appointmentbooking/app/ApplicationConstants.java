package com.finalyear.appointmentbooking.app;

import android.content.Context;
import android.net.ConnectivityManager;

/**
 * Holds application constants which will be used throughout the application
 *
 * @author Natasha Whitter <Natasha Whitter>
 * @version 1.0
 * @since 19/03/14
 */
public class ApplicationConstants
{
    // FORMATS
    public static String DEFAULT_DATE = "dd/MM/yyyy";
    public static String SHORT_DATE = "dd/MM/yy";
    public static String LONG_DATE = "dd:MMM:yyyy";

    public static String DEFAULT_DATETIME = "dd/MM/yyyy HH:mm:ss";
    public static String SHORT_DATETIME = "dd/MM/yy HH:mm:ss";
    public static String LONG_DATETIME = "dd/MMM/yyyy HH:mm:ss";

    public static String SHORT_TIME = "HH:mm:ss";
    public static String DEFAULT_TIME_24HR = "HH:mm";
    public static String DEFAULT_TIME_12HR = "hh:mm a";
    public static String LONG_TIME = "HH:mm:ss:mm";

    public static boolean isNetworkConnected(Context context)
    {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return (cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isAvailable() && cm.getActiveNetworkInfo().isConnected());
    }
}
