package com.finalyear.appointmentbooking.app.ui.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import com.finalyear.appointmentbooking.app.ui.RegistrationAccountFragment;
import com.finalyear.appointmentbooking.app.ui.RegistrationPracticeFragment;

/**
 * @author Natasha Whitter <Natasha Whitter>
 * @version 1.0
 * @since 25/04/2014
 */
public class RegistrationPagerAdapter extends FragmentPagerAdapter
{
    final int PAGE_COUNT = 2;

    public RegistrationPagerAdapter(FragmentManager fm)
    {
        super(fm);
    }

    @Override
    public Fragment getItem(int position)
    {
        switch (position)
        {
            case 0:
                return new RegistrationPracticeFragment();
            case 1:
                return new RegistrationAccountFragment();
            default:
                return null;
        }
    }

    @Override
    public int getCount()
    {
        return PAGE_COUNT;
    }
}
