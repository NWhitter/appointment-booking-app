package com.finalyear.appointmentbooking.app.ui.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.finalyear.appointmentbooking.app.R;
import com.finalyear.appointmentbooking.app.data.Practice;
import com.parse.ParseQuery;
import com.parse.ParseQueryAdapter;

/**
 * @author Natasha Whitter <Natasha Whitter>
 * @version 1.0
 * @since 25/04/2014
 */
public class PracticeAdapter extends ParseQueryAdapter<Practice>
{
    /**
     * Creates a query which gets list of all practices ordered ascending
     * @param context - activity which assigns adapter
     */
    public PracticeAdapter(Context context)
    {
        super(context, new ParseQueryAdapter.QueryFactory<Practice>()
        {
            @Override
            public ParseQuery<Practice> create()
            {
                ParseQuery<Practice> query = ParseQuery.getQuery("Practice");
                query.setCachePolicy(ParseQuery.CachePolicy.NETWORK_ELSE_CACHE);
                query.include("address");
                query.orderByAscending("name");
                return query;
            }
        });
    }

    @Override
    public View getItemView(final Practice practice, View v, ViewGroup parent)
    {
        if (v == null)
        {
            v = View.inflate(getContext(), R.layout.item_list_practice, null);
        }

        super.getItemView(practice, v, parent);

        TextView tvName = (TextView) v.findViewById(R.id.practice_name);
        TextView tvLocation = (TextView) v.findViewById(R.id.practice_location);
        TextView tvTelephone = (TextView) v.findViewById(R.id.practice_telephone);

        tvName.setText(practice.getName());
        tvLocation.setText(practice.getAddress().getStreet() + ", " + practice.getAddress().getCounty());
        // tvTelephone.setText("Telephone: " + practice.getTelephone());

        return v;
    }
}
