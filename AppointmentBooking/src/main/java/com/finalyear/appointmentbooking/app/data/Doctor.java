package com.finalyear.appointmentbooking.app.data;

import com.finalyear.appointmentbooking.app.data.interfaces.IPerson;
import com.parse.ParseClassName;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.io.Serializable;
import java.util.Date;

/**
 * This Doctor class is an extension of ParseObject to access Doctor objects
 *
 * @author Natasha Whitter <Natasha Whitter>
 * @version 1.0
 * @see com.finalyear.appointmentbooking.app.data.interfaces.IPerson
 * @since 08/04/2014
 */
@ParseClassName("Doctor")
public class Doctor extends ParseObject implements IPerson, Serializable
{
    /**
     * Empty constructor needed for ParseObject class
     */
    public Doctor()
    {
    }

    /**
     * Gets the class which the query will use to access Doctor data.
     *
     * @return query to fetch Doctor objects
     * @see com.parse.ParseQuery
     */
    public static ParseQuery<Doctor> getQuery()
    {
        return ParseQuery.getQuery(Doctor.class);
    }

    /**
     * Gets the title name of this Doctor.
     *
     * @return this Doctor's title
     * @see com.finalyear.appointmentbooking.app.data.interfaces.IPerson#getTitle()
     */
    @Override
    public String getTitle()
    {
        return super.getString("title");
    }

    /**
     * Changes the title of this Doctor.
     *
     * @param title This Doctor's new title
     * @see com.finalyear.appointmentbooking.app.data.interfaces.IPerson#setTitle(String)
     */
    @Override
    public void setTitle(String title)
    {
        super.put("title", title);
    }

    /**
     * Gets the first name of this Doctor.
     *
     * @return this Doctor's first name
     * @see com.finalyear.appointmentbooking.app.data.interfaces.IPerson#getFirstName()
     */
    @Override
    public String getFirstName()
    {
        return super.getString("firstName");
    }

    /**
     * Changes the first name of this Doctor.
     *
     * @param firstName This Doctor's new first name
     * @see com.finalyear.appointmentbooking.app.data.interfaces.IPerson#setFirstName(String)
     */
    @Override
    public void setFirstName(String firstName)
    {
        super.put("firstName", firstName);
    }

    /**
     * Gets the surname of this Doctor.
     *
     * @return this Doctor's surname
     * @see com.finalyear.appointmentbooking.app.data.interfaces.IPerson#getSurname()
     */
    @Override
    public String getSurname()
    {
        return super.getString("surname");
    }

    /**
     * Changes the surname of this Doctor.
     *
     * @param surname This Doctor's new surname
     * @see com.finalyear.appointmentbooking.app.data.interfaces.IPerson#setSurname(String)
     */
    @Override
    public void setSurname(String surname)
    {
        super.put("surname", surname);
    }

    /**
     * Gets the date of birth of this Doctor.
     *
     * @return this Doctor's date of birth
     * @see com.finalyear.appointmentbooking.app.data.interfaces.IPerson#getDateOfBirth()
     */
    @Override
    public Date getDateOfBirth()
    {
        return super.getDate("dateOfBirth");
    }

    /**
     * Changes the date of birth of this Doctor.
     *
     * @param birthDate This Doctor's new date of birth
     * @see com.finalyear.appointmentbooking.app.data.interfaces.IPerson#setDateOfBirth(java.util.Date)
     */
    @Override
    public void setDateOfBirth(Date birthDate)
    {
        super.put("dateOfBirth", birthDate);
    }

    /**
     * Gets the gender of this Doctor.
     *
     * @return this Doctor's gender
     * @see com.finalyear.appointmentbooking.app.data.interfaces.IPerson#getGender()
     */
    @Override
    public String getGender()
    {
        return super.getString("gender");
    }

    /**
     * Changes the gender of this Doctor.
     *
     * @param gender This Doctor's new gender
     * @see com.finalyear.appointmentbooking.app.data.interfaces.IPerson#setGender(String)
     */
    @Override
    public void setGender(String gender)
    {
        super.put("gender", gender);
    }

    /**
     * Gets the position of this Doctor.
     *
     * @return this Doctor's position
     */
    public String getPosition()
    {
        return super.getString("position");
    }

    /**
     * Changes the position of this Doctor.
     *
     * @param position This Doctor's new position
     */
    public void setPosition(String position)
    {
        super.put("position", position);
    }

    /**
     * Gets the speciality of this Doctor.
     *
     * @return this Doctor's speciality
     */
    public String getSpeciality()
    {
        return super.getString("speciality");
    }

    /**
     * Changes the speciality of this Doctor.
     *
     * @param speciality This Doctor's new position
     */
    public void setSpeciality(String speciality)
    {
        super.put("speciality", speciality);
    }

    /**
     * Gets the education of this Doctor.
     *
     * @return this Doctor's education
     */
    public String getEducation()
    {
        return super.getString("education");
    }

    /**
     * Changes the education of this Doctor.
     *
     * @param education This Doctor's new education
     */
    public void getEducation(String education)
    {
        super.put("education", education);
    }

    /**
     * Gets the office of this Doctor.
     *
     * @return this Doctor's office
     */
    public String getOffice()
    {
        return super.getString("office");
    }

    /**
     * Changes the office of this Doctor.
     *
     * @param office This Doctor's new office
     */
    public void setOffice(String office)
    {
        super.put("office", office);
    }

    /**
     * Gets the practice assigned to this Appointment.
     *
     * @return this Doctor's assigned practice
     */
    @Override
    public ParseObject getPractice()
    {
        return super.getParseObject("practice");
    }

    /**
     * Changes the practice assigned to this Doctor
     *
     * @param GPPractice This Doctor's new GP Practice
     */
    @Override
    public void setPractice(ParseObject GPPractice)
    {
        super.put("practice", GPPractice);
    }

    @Override
    public String toString()
    {
        return getTitle() + " " + getFirstName() + " " + getSurname();
    }
}
