/**
 *
 */
package com.finalyear.appointmentbooking.app;

import android.app.Application;
import android.content.SharedPreferences;
import com.finalyear.appointmentbooking.app.data.*;
import com.finalyear.appointmentbooking.app.nav.NavigationDrawer;
import com.parse.*;

import java.util.TimeZone;

/**
 * Add info here
 *
 * @author Natasha Whitter
 * @version 1.0
 * @since 21/02/2014
 */
public class ParseApplication extends Application
{
    private static final String TAG = "ParseApplication";

    public static SharedPreferences preferences;

    @Override
    public void onCreate()
    {
        super.onCreate();

        // Register the subclass tables here
        ParseObject.registerSubclass(Address.class);
        ParseObject.registerSubclass(Appointment.class);
        ParseObject.registerSubclass(AppointmentDate.class);
        ParseObject.registerSubclass(Doctor.class);
        ParseObject.registerSubclass(DoctorSchedule.class);
        ParseObject.registerSubclass(Practice.class);
        ParseObject.registerSubclass(Slot.class);
        ParseObject.registerSubclass(User.class);

        // Add your initialisation code here
        Parse.initialize(this, /*Add application key here*/, /*Add client key here*/);

        ParseACL defaultACL = new ParseACL();

        // If you would like objects to be private by default, remove this line
        defaultACL.setPublicReadAccess(false);
        defaultACL.setPublicWriteAccess(false);

        TimeZone.setDefault(TimeZone.getTimeZone("BST"));

        PushService.setDefaultPushCallback(this, NavigationDrawer.class);
        ParseInstallation.getCurrentInstallation().saveInBackground();

        // PreferenceManager.setDefaultValues(this, resId, false);
        // preferences = PreferenceManager.getDefaultSharedPreferences(this);

        ParseACL.setDefaultACL(defaultACL, true);
    }
}
