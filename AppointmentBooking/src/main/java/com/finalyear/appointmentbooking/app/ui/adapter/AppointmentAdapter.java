package com.finalyear.appointmentbooking.app.ui.adapter;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.media.Image;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.finalyear.appointmentbooking.app.ApplicationConstants;
import com.finalyear.appointmentbooking.app.R;
import com.finalyear.appointmentbooking.app.data.Appointment;
import com.parse.DeleteCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseQueryAdapter;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.TimeZone;

/**
 * @author Natasha Whitter <Natasha Whitter>
 * @version 1.0
 * @since 29/03/2014
 * TODO: Somehow change query to ArrayAdapter so it can be ordered by slot
 */
public class AppointmentAdapter extends ParseQueryAdapter<Appointment>
{
    public AppointmentAdapter(Context context)
    {
        super(context, new ParseQueryAdapter.QueryFactory<Appointment>()
        {
            public ParseQuery<Appointment> create()
            {
                ParseQuery<Appointment> query = ParseQuery.getQuery("Appointment");
                query.setCachePolicy(ParseQuery.CachePolicy.NETWORK_ELSE_CACHE);
                Calendar today = Calendar.getInstance(TimeZone.getDefault());
                today.set(Calendar.HOUR_OF_DAY, 0);
                today.set(Calendar.SECOND, 0);
                today.set(Calendar.MINUTE, 0);
                today.set(Calendar.MILLISECOND, 0);
                query.whereGreaterThanOrEqualTo("appointmentDate", today.getTime());
                query.include("doctor");
                query.include("slot");
                query.orderByAscending("appointmentDate");
                return query;
            }
        });
    }

    @Override
    public View getItemView(final Appointment appointment, View v, ViewGroup parent)
    {
        if (v == null)
        {
            v = View.inflate(getContext(), R.layout.item_list_appointments, null);
        }

        super.getItemView(appointment, v, parent);

        TextView tvDate = (TextView) v.findViewById(R.id.appointment_date);
        TextView tvDoctor = (TextView) v.findViewById(R.id.doctor_name);
        ImageView ivCancel = (ImageView) v.findViewById(R.id.cancel_appointment);

        ivCancel.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if (ApplicationConstants.isNetworkConnected(getContext()))
                {
                    cancelAppointment(appointment);
                }
                else
                {
                    Toast.makeText(getContext(), "Please connect to the internet to cancel your appointment", Toast.LENGTH_SHORT).show();
                }
            }
        });

        Calendar cal = Calendar.getInstance();
        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(appointment.getAppointmentDate());
        cal.setTimeInMillis(appointment.getSlot().getStartTime());

        if (cal2.get(Calendar.DAY_OF_MONTH) == Calendar.getInstance().get(Calendar.DAY_OF_MONTH)
                && cal2.get(Calendar.MONTH) == Calendar.getInstance().get(Calendar.MONTH)
                && cal2.get(Calendar.YEAR) == Calendar.getInstance().get(Calendar.YEAR))
        {
            String appointmentDate = "Today";
            String appointmentTime = new SimpleDateFormat(ApplicationConstants.DEFAULT_TIME_12HR).format(cal.getTime());
            tvDate.setText(appointmentDate + " " + appointmentTime);
            ivCancel.setVisibility(View.GONE);
        }
        else
        {
            String appointmentDate = new SimpleDateFormat(ApplicationConstants.DEFAULT_DATE).format(appointment.getAppointmentDate());
            String appointmentTime = new SimpleDateFormat(ApplicationConstants.DEFAULT_TIME_12HR).format(cal.getTime());
            tvDate.setText(appointmentDate + " " + appointmentTime);
        }

        tvDoctor.setText(appointment.getDoctor().toString()+ " - Room: " + appointment.getDoctor().getOffice());

        return v;
    }

    public void cancelAppointment(final Appointment appointment)
    {
        new AlertDialog.Builder(getContext()).setTitle("Delete Appointment")
                .setMessage("Are you sure you would like to delete this appointment?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {

                        final ProgressDialog signUpDialog = new ProgressDialog(getContext());
                        signUpDialog.setIndeterminate(true);
                        signUpDialog.setMessage("Cancelling appointment. Please wait.");
                        signUpDialog.show();

                        appointment.deleteInBackground(new DeleteCallback()
                        {
                            @Override
                            public void done(ParseException e)
                            {
                                signUpDialog.dismiss();
                                if (e == null)
                                {
                                    Toast.makeText(getContext(), "Appointment successful cancelled", Toast.LENGTH_SHORT).show();
                                    loadObjects();
                                } else
                                {
                                    Toast.makeText(getContext(), "Appointment not cancelled", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
                    }
                })
                .setNegativeButton(android.R.string.no, null)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }
}
