package com.finalyear.appointmentbooking.app.ui;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.finalyear.appointmentbooking.app.ApplicationConstants;
import com.finalyear.appointmentbooking.app.R;
import com.finalyear.appointmentbooking.app.Validation;
import com.finalyear.appointmentbooking.app.data.Practice;
import com.finalyear.appointmentbooking.app.data.User;
import com.finalyear.appointmentbooking.app.ui.dialog.BirthDateDialog;
import com.finalyear.appointmentbooking.app.ui.dialog.DateDialog;
import com.finalyear.appointmentbooking.app.ui.dialog.PasswordInformationDialog;
import com.finalyear.appointmentbooking.app.ui.interfaces.IRegisterClickListener;
import com.parse.ParseACL;
import com.parse.ParseException;
import com.parse.SignUpCallback;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.TimeZone;

/**
 * @author Natasha Whitter <Natasha Whitter>
 * @version 1.0
 * @since 28/04/2014
 */
public class RegistrationAccountFragment extends Fragment
{
    SimpleDateFormat dateFormat = new SimpleDateFormat(ApplicationConstants.DEFAULT_DATE);
    private IRegisterClickListener listener;
    private Practice practice;
    private EditText tvFirstName;
    private EditText tvSurname;
    private EditText tvEmail;
    private EditText tvPassword;
    private EditText tvPasswordAgain;
    private Button btnBirthDate;
    private Button btnSignUp;
    private TextView tvSignIn;
    private ImageView ivHint;
    private Calendar birthDate;
    private BirthDateDialog dateDialog;
    DatePickerDialog.OnDateSetListener onDate = new DatePickerDialog.OnDateSetListener()
    {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth)
        {
            birthDate = Calendar.getInstance(Locale.getDefault());
            birthDate.set(year, monthOfYear, dayOfMonth, 0, 0, 0);
            btnBirthDate.setText(new StringBuilder(dateFormat.format(birthDate.getTime())));
            if (dateDialog != null)
            {
                dateDialog.dismiss();
            }
        }
    };
    private int year;
    private int month;
    private int day;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_account_details, container, false);

        tvFirstName = (EditText) view.findViewById(R.id.reg_first_name);
        tvSurname = (EditText) view.findViewById(R.id.reg_surname);
        tvEmail = (EditText) view.findViewById(R.id.reg_email);
        tvPassword = (EditText) view.findViewById(R.id.reg_password);
        tvPasswordAgain = (EditText) view.findViewById(R.id.reg_password_again);
        btnBirthDate = (Button) view.findViewById(R.id.reg_birth_date);
        btnSignUp = (Button) view.findViewById(R.id.reg_create_account);
        tvSignIn = (TextView) view.findViewById(R.id.reg_sign_in);
        ivHint = (ImageView) view.findViewById(R.id.reg_hint);

        if (savedInstanceState != null)
        {
            birthDate = (Calendar) savedInstanceState.getSerializable("date");
            practice = (Practice) savedInstanceState.getSerializable("practice");
        }

        if (birthDate != null)
        {
            year = birthDate.get(Calendar.YEAR);
            month = birthDate.get(Calendar.MONTH);
            day = birthDate.get(Calendar.DAY_OF_MONTH);
        } else
        {
            birthDate = Calendar.getInstance(TimeZone.getDefault());
            year = birthDate.get(Calendar.YEAR);
            month = birthDate.get(Calendar.MONTH);
            day = birthDate.get(Calendar.DAY_OF_MONTH);
        }

        btnBirthDate.setText(new StringBuilder(dateFormat.format(birthDate.getTime())));

        btnBirthDate.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                showDatePicker();
            }
        });

        btnSignUp.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                createUser();
            }
        });
        tvSignIn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent i = new Intent(getActivity(), LoginActivity.class);
                startActivity(i);
                getActivity().finish();
            }
        });

        ivHint.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                PasswordInformationDialog dialog = new PasswordInformationDialog();
                dialog.show(getFragmentManager(), "dateOfBirthDialog");
            }
        });

        return view;
    }

    public void createUser()
    {
        if (validateInformation())
        {
            if (practice != null)
            {
                User user = new User();
                user.setFirstName(tvFirstName.getText().toString().trim());
                user.setSurname(tvSurname.getText().toString().trim());
                user.setDateOfBirth(birthDate.getTime());
                user.setPassword(tvPassword.getText().toString().trim());
                user.setUsername(tvEmail.getText().toString().toLowerCase().trim());
                user.setPractice(practice);
                ParseACL acl = new ParseACL();
                acl.setPublicWriteAccess(false);
                acl.setPublicWriteAccess(false);
                user.setACL(acl);

                final ProgressDialog signUpDialog = new ProgressDialog(getActivity());
                signUpDialog.setIndeterminate(true);
                signUpDialog.setMessage("Creating account. Please wait.");
                signUpDialog.show();

                user.signUpInBackground(new SignUpCallback()
                {
                    @Override
                    public void done(ParseException e)
                    {
                        signUpDialog.dismiss();
                        if (e == null)
                        {

                            Toast.makeText(getActivity(), "Account created", Toast.LENGTH_SHORT).show();
                            Intent i = new Intent(getActivity(), LoginActivity.class);
                            startActivity(i);
                            getActivity().finish();
                        } else
                        {
                            Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            } else
            {
                Toast.makeText(getActivity(), "Practice doesn't exist", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void showDatePicker()
    {
        dateDialog = new BirthDateDialog(onDate);
        Bundle args = new Bundle();
        dateDialog.setArguments(args);
        if (birthDate == null)
        {
            Calendar today = Calendar.getInstance(TimeZone.getDefault());
            today.set(today.get(Calendar.YEAR), today.get(Calendar.MONTH), today.get(Calendar.DAY_OF_MONTH), 0, 0, 0);
            args.putInt("year", today.get(Calendar.YEAR));
            args.putInt("month", today.get(Calendar.MONTH));
            args.putInt("day", today.get(Calendar.DAY_OF_MONTH));

        } else
        {
            args.putInt("year", birthDate.get(Calendar.YEAR));
            args.putInt("month", birthDate.get(Calendar.MONTH));
            args.putInt("day", birthDate.get(Calendar.DAY_OF_MONTH));
        }

        dateDialog.setArguments(args);
        dateDialog.setCallBack(onDate);
        dateDialog.show(getFragmentManager(), "Select Date of Birth");
    }

    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(getActivity());

        try
        {
            listener = (IRegisterClickListener) getActivity();
        } catch (ClassCastException e)
        {
            throw new ClassCastException(activity.toString() + " must implement IRegisterClickListener");
        }
    }

    public void setPractice(final Practice practice)
    {
        this.practice = practice;
    }

    @Override
    public void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);
        outState.putSerializable("practice", this.practice);
    }

    public boolean validateInformation()
    {
        if (Validation.isEmpty(tvFirstName))
        {
            Toast.makeText(getActivity(), getResources().getString(R.string.error_invalid_first_name), Toast.LENGTH_SHORT).show();
            return false;
        }
        else if (!Validation.hasOnlyLetters(tvFirstName))
        {
            Toast.makeText(getActivity(), getResources().getString(R.string.error_fname_valid), Toast.LENGTH_SHORT).show();
            return false;
        }
        else if (Validation.isEmpty(tvSurname))
        {
            Toast.makeText(getActivity(), getResources().getString(R.string.error_invalid_surname), Toast.LENGTH_SHORT).show();
            return false;
        }
        else if (!Validation.hasOnlyLetters(tvSurname))
        {
            Toast.makeText(getActivity(), getResources().getString(R.string.error_surname_not_valid), Toast.LENGTH_SHORT).show();
            return false;
        }
        else if (Validation.isEmpty(tvEmail))
        {
            Toast.makeText(getActivity(), getResources().getString(R.string.error_email_required), Toast.LENGTH_SHORT).show();
            return false;
        }
        else if (!Validation.isValidEmail(tvEmail))
        {
            Toast.makeText(getActivity(), getResources().getString(R.string.error_invalid_email), Toast.LENGTH_SHORT).show();
            return false;
        } else if (!Validation.isValidPassword(tvPassword))
        {
            Toast.makeText(getActivity(), getResources().getString(R.string.error_invalid_password), Toast.LENGTH_SHORT).show();
            return false;
        }
        else if (Validation.isEmpty(tvPassword))
        {
            Toast.makeText(getActivity(), getResources().getString(R.string.error_password_required), Toast.LENGTH_SHORT).show();
            return false;
        }
        else if (Validation.isEmpty(tvPasswordAgain))
        {
            Toast.makeText(getActivity(), getResources().getString(R.string.error_password_again), Toast.LENGTH_SHORT).show();
            return false;
        } else if (!Validation.isPasswordsMatching(tvPassword, tvPasswordAgain))
        {
            Toast.makeText(getActivity(), getResources().getString(R.string.error_password_match), Toast.LENGTH_SHORT).show();
            return false;
        } else if (!Validation.is18(birthDate))
        {
            Toast.makeText(getActivity(), getResources().getText(R.string.error_invalid_age), Toast.LENGTH_SHORT).show();
            return false;
        } else if (!ApplicationConstants.isNetworkConnected(getActivity()))
        {
            Toast.makeText(getActivity(), "Please connect to the internet to create an account", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }
}
