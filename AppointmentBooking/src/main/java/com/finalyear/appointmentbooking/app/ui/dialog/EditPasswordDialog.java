package com.finalyear.appointmentbooking.app.ui.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import com.finalyear.appointmentbooking.app.R;
import com.finalyear.appointmentbooking.app.Validation;
import com.finalyear.appointmentbooking.app.data.User;
import com.parse.ParseException;
import com.parse.SaveCallback;

/**
 * @author Natasha Whitter <Natasha Whitter>
 * @version 1.0
 * @since 27/04/2014
 */
public class EditPasswordDialog extends DialogFragment
{
    private EditText etPassword;
    private EditText etNewPassword;
    private User user;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState)
    {
        AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
        View view = getActivity().getLayoutInflater().inflate(R.layout.fragment_dialog_edit_password, null);
        etPassword = (EditText) view.findViewById(R.id.current_password);
        etPassword.setVisibility(View.GONE);
        etNewPassword = (EditText) view.findViewById(R.id.new_password);
        user = (User) User.getCurrentUser();

        dialog.setView(view);
        dialog.setTitle("Change Password");
        dialog.setPositiveButton("OK", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int position)
            {
                if (Validation.isValidPassword(etNewPassword) && !Validation.isEmpty(etNewPassword))
                {
                    savePassword();
                    dialog.dismiss();
                } else
                {
                    Toast.makeText(getActivity(), "Incorrect password entered", Toast.LENGTH_SHORT).show();
                }
            }
        });

        dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int position)
            {
                getTargetFragment().onActivityResult(getTargetRequestCode(), getActivity().RESULT_CANCELED, null);
                dialog.cancel();
            }
        });

        return dialog.create();
    }

    public void savePassword()
    {
        user.setPassword(etNewPassword.getText().toString());
        final ProgressDialog dialog = new ProgressDialog(getActivity());
        dialog.setIndeterminate(true);
        dialog.setMessage("Changing password. Please wait.");
        dialog.show();
        user.saveInBackground(new SaveCallback()
        {
            @Override
            public void done(ParseException e)
            {
                dialog.dismiss();
                if (e == null)
                {
                    getTargetFragment().onActivityResult(getTargetRequestCode(), getActivity().RESULT_OK, null);
                }
            }
        });
    }

}
