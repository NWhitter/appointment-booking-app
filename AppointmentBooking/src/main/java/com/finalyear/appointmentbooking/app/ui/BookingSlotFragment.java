package com.finalyear.appointmentbooking.app.ui;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.GridView;
import android.widget.TextView;
import com.finalyear.appointmentbooking.app.R;
import com.finalyear.appointmentbooking.app.data.Appointment;
import com.finalyear.appointmentbooking.app.data.Doctor;
import com.finalyear.appointmentbooking.app.data.Slot;
import com.finalyear.appointmentbooking.app.data.User;
import com.finalyear.appointmentbooking.app.ui.adapter.SlotAdapter;
import com.finalyear.appointmentbooking.app.ui.dialog.DateDialog;
import com.finalyear.appointmentbooking.app.ui.interfaces.IBookingClickListener;
import com.parse.ParseUser;

import java.util.Calendar;
import java.util.Locale;
import java.util.TimeZone;

/**
 * @author Natasha Whitter <Natasha Whitter>
 * @version 1.0
 * @since 17/04/2014
 */
public class BookingSlotFragment extends Fragment
{
    IBookingClickListener listener;

    private TextView tvDates;
    private GridView gvSlots;
    private SlotAdapter adapter;
    private DateDialog dateDialog;

    private Calendar appointmentDate;

    // TODO: Application crashes when resumed as doctor is null

    DatePickerDialog.OnDateSetListener onDate = new DatePickerDialog.OnDateSetListener()
    {
        @Override
        public void onDateSet(DatePicker view, int year, int month, int day)
        {
            String date = String.valueOf(day) + "/" + String.valueOf(month + 1) + "/" + String.valueOf(year);

            tvDates.setText(date);

            appointmentDate = Calendar.getInstance(Locale.getDefault());
            appointmentDate.set(Calendar.YEAR, year);
            appointmentDate.set(Calendar.MONTH, month);
            appointmentDate.set(Calendar.DAY_OF_MONTH, day);
            appointmentDate.set(Calendar.HOUR_OF_DAY, 0);
            appointmentDate.set(Calendar.MINUTE, 0);
            appointmentDate.set(Calendar.SECOND, 0);
            appointmentDate.set(Calendar.MILLISECOND, 0);

            if (dateDialog != null)
            {
                dateDialog.dismiss();
            }

            showProgress();
        }
    };
    private Doctor doctor;
    private int year;
    private int month;
    private int day;

    public void showProgress()
    {
        if (adapter != null)
        {
            final ProgressDialog dialog = new ProgressDialog(getActivity());
            dialog.setCancelable(true);
            dialog.setMessage("Updating available slots...");
            dialog.setIndeterminate(true);
            dialog.show();

            Runnable runnable = new Runnable()
            {
                @Override
                public void run()
                {
                    adapter.updateAdapter(appointmentDate, doctor);
                    getActivity().runOnUiThread(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            adapter.notifyDataSetChanged();
                            dialog.dismiss();
                        }
                    });
                }
            };
            new Thread(runnable).start();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_booking_available_time, container, false);

        if (savedInstanceState != null)
        {
            appointmentDate = (Calendar) savedInstanceState.getSerializable("date");
            doctor = (Doctor) savedInstanceState.getSerializable("doctor");
        }

        tvDates = (TextView) view.findViewById(R.id.appointment_date);
        gvSlots = (GridView) view.findViewById(R.id.appointment_slots);
        gvSlots.setEmptyView(view.findViewById(R.id.empty_element));

        if (appointmentDate != null)
        {
            year = appointmentDate.get(Calendar.YEAR);
            month = appointmentDate.get(Calendar.MONTH);
            day = appointmentDate.get(Calendar.DAY_OF_MONTH);
        } else
        {
            appointmentDate = Calendar.getInstance(TimeZone.getDefault());
            appointmentDate.add(Calendar.DAY_OF_MONTH, 1);
            year = appointmentDate.get(Calendar.YEAR);
            month = appointmentDate.get(Calendar.MONTH);
            day = appointmentDate.get(Calendar.DAY_OF_MONTH);
        }

        tvDates.setText(new StringBuilder().append(day).append("/").append(month + 1).append("/").append(year));

        if (doctor != null)
        {
            adapter = new SlotAdapter(getActivity(), appointmentDate, doctor);
            gvSlots.setAdapter(adapter);
        }

        tvDates.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                showDatePicker();
            }
        });

        gvSlots.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {

                Slot slot = adapter.getItem(position);
                Appointment newBooking = new Appointment();
                newBooking.setAppointmentDate(appointmentDate.getTime());
                newBooking.setDoctor(doctor);
                newBooking.setSlot(slot);
                newBooking.setPatient((User) ParseUser.getCurrentUser());
                newBooking.setStatus("Not booked");
                listener.onListClick(newBooking);
                listener.nextFragment();
            }
        });

        return view;
    }

    @Override
    public void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);
        outState.putSerializable("date", this.appointmentDate);
        outState.putSerializable("doctor", this.doctor);
    }

    private void showDatePicker()
    {
        // Set the dialog to the current date
        dateDialog = new DateDialog(onDate, getActivity());

        Calendar currentDay = Calendar.getInstance(Locale.getDefault());
        currentDay.set(Calendar.HOUR_OF_DAY, 0);
        currentDay.set(Calendar.MILLISECOND, 0);
        currentDay.set(Calendar.SECOND, 0);
        currentDay.set(Calendar.MILLISECOND, 0);

        Bundle args = new Bundle();
        if (appointmentDate == null)
        {
            args.putInt("year", currentDay.get(Calendar.YEAR));
            args.putInt("month", currentDay.get(Calendar.MONTH));
            args.putInt("day", currentDay.get(Calendar.DAY_OF_MONTH));
        } else
        {
            args.putInt("year", appointmentDate.get(Calendar.YEAR));
            args.putInt("month", appointmentDate.get(Calendar.MONTH));
            args.putInt("day", appointmentDate.get(Calendar.DAY_OF_MONTH));
        }
        dateDialog.setArguments(args);

        // sent back to dialog and show
        dateDialog.setCallBack(onDate);
        dateDialog.show(getFragmentManager(), "Select appointment date");
    }

    public void setDoctor(final Doctor doctor)
    {
        this.doctor = doctor;
        appointmentDate = Calendar.getInstance(TimeZone.getDefault());
        appointmentDate.add(Calendar.DAY_OF_MONTH, 1);
        adapter = new SlotAdapter(getActivity(), appointmentDate, this.doctor);
        gvSlots.setAdapter(adapter);
    }

    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);

        try
        {
            listener = (IBookingClickListener) getActivity();
        } catch (ClassCastException e)
        {
            throw new ClassCastException(activity.toString() + " must implement IBookingClickListener");
        }
    }
}