package com.finalyear.appointmentbooking.app.ui;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.*;
import android.widget.AdapterView;
import android.widget.Toast;
import com.finalyear.appointmentbooking.app.ApplicationConstants;
import com.finalyear.appointmentbooking.app.R;
import com.finalyear.appointmentbooking.app.data.Appointment;
import com.finalyear.appointmentbooking.app.ui.adapter.AppointmentAdapter;
import com.parse.DeleteCallback;
import com.parse.ParseException;
import com.parse.ParseQueryAdapter;

public class AppointmentsFragment extends ListFragment implements AdapterView.OnItemClickListener
{
    private static final int CONTEXT_MENU_VIEW_APP = 1;
    private static final int CONTEXT_MENU_CANCEL_APP = 2;
    private static final int REQUEST_CODE_BOOK_APPOINTMENT = 100;
    private String TAG = "AppointmentsFragment";
    private ParseQueryAdapter<Appointment> adapter;
    private ProgressDialog dialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        setHasOptionsMenu(true);
        adapter = new AppointmentAdapter(getActivity());
        setListAdapter(adapter);
        Log.d(TAG, String.valueOf(adapter.getCount()));
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onStart()
    {
        super.onStart();
        setEmptyText("No Booked Appointments Available");
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo)
    {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.setHeaderTitle("Appointment");
        menu.add(Menu.NONE, CONTEXT_MENU_VIEW_APP, 0, "View Appointment");
    }

    @Override
    public boolean onContextItemSelected(MenuItem item)
    {
        // Get extra info about list item that was long-pressed
        final AdapterView.AdapterContextMenuInfo menuInfo = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();

        switch (item.getItemId())
        {
            case CONTEXT_MENU_VIEW_APP:
                viewAppointmentDetails(adapter.getItem(menuInfo.position));
                break;
        }
        return super.onContextItemSelected(item);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
    {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.view_appointments, menu);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);
        registerForContextMenu(getListView());
        getListView().setOnItemClickListener(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case R.id.book_appointment:
                if (ApplicationConstants.isNetworkConnected(getActivity()))
                {
                    Intent i = new Intent(getActivity(), BookingActivity.class);
                    startActivityForResult(i, REQUEST_CODE_BOOK_APPOINTMENT);
                    return true;
                } else
                {
                    Toast.makeText(getActivity(), "Please connect to the internet to book an appointment", Toast.LENGTH_SHORT).show();
                }
            case R.id.refresh_appointments:
                updateAppointmentList();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void updateAppointmentList()
    {
        adapter.loadObjects();
        setListAdapter(adapter);
    }

    @Override
    public void onResume()
    {
        super.onResume();
        updateAppointmentList();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id)
    {
        viewAppointmentDetails(adapter.getItem(position));
    }

    public void viewAppointmentDetails(Appointment appointment)
    {
        Intent intent = new Intent(getActivity(), AppointmentDetailsActivity.class);
        intent.putExtra("appointment", appointment.getObjectId());
        intent.putExtra("isPast", false);
        startActivity(intent);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (requestCode == REQUEST_CODE_BOOK_APPOINTMENT)
        {
            if (resultCode == getActivity().RESULT_OK)
            {
                updateAppointmentList();
            }
        }
    }
}